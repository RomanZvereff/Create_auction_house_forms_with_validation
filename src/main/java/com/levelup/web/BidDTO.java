package com.levelup.web;

import com.sun.istack.internal.NotNull;
import lombok.Data;
import lombok.NonNull;

@Data
public class BidDTO {

    @NonNull
    private int bid_size;

    @NonNull
    private int fk_product_id;

    @NotNull
    private int fk_buyer_id;

    public int getBid_size() {
        return bid_size;
    }

    public int getFk_product_id() {
        return fk_product_id;
    }

    public int getFk_buyer_id() {
        return fk_buyer_id;
    }

    public void setBid_size(int bid_size) {
        this.bid_size = bid_size;
    }

    public void setFk_product_id(int fk_product_id) {
        this.fk_product_id = fk_product_id;
    }

    public void setFk_buyer_id(int fk_buyer_id) {
        this.fk_buyer_id = fk_buyer_id;
    }
}
