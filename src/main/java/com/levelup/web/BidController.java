package com.levelup.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;

@Controller
public class BidController {

    @RequestMapping(value = "/static/formBid", method = RequestMethod.GET)
    public String getFormBid(@RequestParam(name = "bid_size") int bid_size,
                             @RequestParam(name = "fk_product_id") int fk_product_id,
                             @RequestParam(name = "fk_buyer_id") int fk_buyer_id) {
        return "/static/success.html";
    }

}