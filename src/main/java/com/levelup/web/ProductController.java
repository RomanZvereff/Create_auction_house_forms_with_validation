package com.levelup.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class ProductController {

    @RequestMapping(value = "/formProduct", method = RequestMethod.GET)
    public String getFormBid(@RequestParam(name = "id") int id,
                             @RequestParam(name = "product_name") String seller_first_name,
                             @RequestParam(name = "product_type") String product_type,
                             @RequestParam(name = "product_address") String product_address,
                             @RequestParam(name = "product_start_price") int product_start_price,
                             @RequestParam(name = "product_date_of_sale") String product_date_of_sale,
                             @RequestParam(name = "product_status") String product_status,
                             @RequestParam(name = "fk_seller_id") int fk_seller_id) {
        return "/static/success.html";
    }

}
