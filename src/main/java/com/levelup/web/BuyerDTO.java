package com.levelup.web;

import lombok.Data;

@Data
public class BuyerDTO {

    private int id;
    private String buyer_first_name;
    private String buyer_last_name;
    private String buyer_phone;
    private int buyer_bid;

    public void setId(int id) {
        this.id = id;
    }

    public void setBuyer_first_name(String buyer_first_name) {
        this.buyer_first_name = buyer_first_name;
    }

    public void setBuyer_last_name(String buyer_last_name) {
        this.buyer_last_name = buyer_last_name;
    }

    public void setBuyer_phone(String buyer_phone) {
        this.buyer_phone = buyer_phone;
    }

    public void setBuyer_bid(int buyer_bid) {
        this.buyer_bid = buyer_bid;
    }

    public int getId() {
        return id;
    }

    public String getBuyer_first_name() {
        return buyer_first_name;
    }

    public String getBuyer_last_name() {
        return buyer_last_name;
    }

    public String getBuyer_phone() {
        return buyer_phone;
    }

    public int getBuyer_bid() {
        return buyer_bid;
    }
}
