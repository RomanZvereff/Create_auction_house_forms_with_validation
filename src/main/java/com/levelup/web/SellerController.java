package com.levelup.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;

@Controller
public class SellerController {

    @RequestMapping(value = "/static/formSeller", method = RequestMethod.GET)
    public String getFormBid(Model model) {
        model.addAttribute("id", "1");
        model.addAttribute("seller_first_name", "Josh");
        model.addAttribute("seller_last_name", "Smith");
        model.addAttribute("seller_phone_number", "555-123-478");
        return "/static/success.html";
    }

}
