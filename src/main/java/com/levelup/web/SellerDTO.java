package com.levelup.web;

import lombok.Data;

@Data
public class SellerDTO {

    private int id;
    private String seller_first_name;
    private String seller_last_name;
    private String seller_phone_number;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setSeller_first_name(String seller_first_name) {
        this.seller_first_name = seller_first_name;
    }

    public void setSeller_last_name(String seller_last_name) {
        this.seller_last_name = seller_last_name;
    }

    public void setSeller_phone_number(String seller_phone_number) {
        this.seller_phone_number = seller_phone_number;
    }

    public String getSeller_first_name() {
        return seller_first_name;
    }

    public String getSeller_last_name() {
        return seller_last_name;
    }

    public String getSeller_phone_number() {
        return seller_phone_number;
    }


}
