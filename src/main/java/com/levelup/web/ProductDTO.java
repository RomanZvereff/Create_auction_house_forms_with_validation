package com.levelup.web;

import lombok.Data;

@Data
public class ProductDTO {

    private int id;
    private String product_name;
    private String product_type;
    private String product_address;
    private int product_start_price;
    private String product_date_of_sale;
    private String product_status;
    private int fk_seller_id;

    public void setId(int id) {
        this.id = id;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public void setProduct_type(String product_type) {
        this.product_type = product_type;
    }

    public void setProduct_address(String product_address) {
        this.product_address = product_address;
    }

    public void setProduct_start_price(int product_start_price) {
        this.product_start_price = product_start_price;
    }

    public void setProduct_date_of_sale(String product_date_of_sale) {
        this.product_date_of_sale = product_date_of_sale;
    }

    public void setProduct_status(String product_status) {
        this.product_status = product_status;
    }

    public void setFk_seller_id(int fk_seller_id) {
        this.fk_seller_id = fk_seller_id;
    }

    public int getId() {
        return id;
    }

    public String getProduct_name() {
        return product_name;
    }

    public String getProduct_type() {
        return product_type;
    }

    public String getProduct_address() {
        return product_address;
    }

    public int getProduct_start_price() {
        return product_start_price;
    }

    public String getProduct_date_of_sale() {
        return product_date_of_sale;
    }

    public String getProduct_status() {
        return product_status;
    }

    public int getFk_seller_id() {
        return fk_seller_id;
    }
}

