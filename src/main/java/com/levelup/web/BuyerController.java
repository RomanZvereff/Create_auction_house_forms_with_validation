package com.levelup.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class BuyerController {

    @RequestMapping(value = "/formBuyer", method = RequestMethod.GET)
    public String getFormBid(@RequestParam(name = "id") int id,
                             @RequestParam(name = "buyer_first_name") String buyer_first_name,
                             @RequestParam(name = "buyer_last_name") String buyer_last_name,
                             @RequestParam(name = "buyer_phone") String buyer_phone,
                             @RequestParam(name = "buyer_bid") int buyer_bid) {
        return "/static/success.html";
    }

}
